# Hydra Testing & Data Analysis Scripts

Scripts for automatic testing of Hydra on FlockLab as well as to automatically analyse FlockLab tests.

| Folder                                 | Description               |
|----------------------------------------|---------------------------|
| [*bin*](./bin)                         | Contains scripts to prepare, schedule, download, analyse and evaluate protocol runs |
| [*notebooks*](./notebooks)             | Contains notebooks to visualize data from real protocol runs |

For more information, please visit the [Hydra wiki](https://gitlab.ethz.ch/tec/public/hydra/hydra-wiki).

## Interface

```
usage: hydra_analysis.py [-h] [-d] [-p STRING] [-t] [-m STRING] [-a] [-r] [-l INT] [-n INT] [-e] [-s]

usage: hydra_analysis.py [options]

optional arguments:
  -h, --help            show this help message and exit
  -d, --download-tests  Download locally scheduled tests.
  -p STRING, --path STRING
                        Path to respective file.
  -t, --test            Schedule new test.
  -m STRING, --message STRING
                        Message which will be added to the XML and the log.
  -a, --add-actuations  Add GPIO actuations stored in '/flocklab_actuation.log'.
  -r, --add-resets      Add GPIO resets stored in '/flocklab_actuation.log'.
  -l INT, --length INT  Test duration.
  -n INT, --network-size INT
                        Network size.
  -e, --measure-energy  Enable power profiling.
  -s, --store-metrics   Store metrics for analysis.
```
